import { Component, OnInit } from '@angular/core';
import { HubConnection} from '@aspnet/signalr-client';
import { timer } from 'rxjs/observable/timer';

@Component({
  selector: 'app-matchmaker',
  templateUrl: './matchmaker.component.html',
  styleUrls: ['./matchmaker.component.css']
})
export class MatchmakerComponent implements OnInit {

  private matchHubConnection: HubConnection;

  messages: string[] = [];

  const counter = timer(1000, 1000);
  timer : number = 0;
  const subscriber;
  constructor() { }

  ngOnInit() {

    this.matchHubConnection = new HubConnection('http://localhost:5000/match');

    this.matchHubConnection
      .start()
      .then(() => console.log('Match Connection started!'))
      .catch(err => console.log('Error while establishing Match connection :('));

    this.matchHubConnection.on('broadcastMessage', (sender: string, receivedMessage: string) => {
      const text = `${sender}: ${receivedMessage}`;
      this.messages.push(text);
    });

    this.matchHubConnection.on('ready', (ip: string) => {
      const text = `MATCH READY: ${ip}`;
      this.messages.push(text);
    });

  }

  startTimer(): void {
    this.subscriber = this.counter.subscribe(val => this.timer = val);
  }

  stopTimer(): void {
    this.subscriber.unsubscribe();
    this.timer = 0;
  }

  find() {
    //console.log(this.matchHubConnection.connection.connectionId + ' is finding');
    this.startTimer();
    this.matchHubConnection
          .invoke('find')
          .catch(err => console.error(err));
  }



}
