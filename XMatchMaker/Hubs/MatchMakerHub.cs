﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using XMatchMaker.Models;

namespace XMatchMaker.Hubs
{
    public class MatchMakerHub : Microsoft.AspNetCore.SignalR.Hub
    {
        public MatchMakerHub()
        {
        }

        // demo pool
        static List<User> users = new List<User>();

        public override System.Threading.Tasks.Task OnConnectedAsync()
        {
            Clients.All.InvokeAsync("broadcastMessage", "MySystem", $"{Context.ConnectionId} joined the conversation.");
            return base.OnConnectedAsync();
        }

        public void find(){
            Clients.All.InvokeAsync("broadcastMessage", "MySystem", $"{Context.ConnectionId} is finding.");
            //TODO: find user in db then add to pools
            User user = new User();
            user.id = Context.ConnectionId;
            user.connectionId = Context.ConnectionId;
            users.Add(user);
            checkPool();
        }

        private void checkPool(){
            //TODO: find 10 users
            //get array of 10 users
            //foreach user, sent notifwication to client

            Console.Out.WriteLine("user number:" + users.Count);
            //demo
            if(users.Count == 10){
                Console.Out.WriteLine("match 10");
                string ip = "dummy ip";
                foreach (User user in users) {
                    Console.Out.WriteLine(user.connectionId);
                    Clients.Client(user.connectionId).InvokeAsync("ready", ip);
                }
            }

        }
    }
}
