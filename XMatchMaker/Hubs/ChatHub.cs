﻿using System;
using Microsoft.AspNetCore.SignalR;

namespace XMatchMaker.Hubs
{
    public class ChatHub : Microsoft.AspNetCore.SignalR.Hub
    {
        public ChatHub()
        {
        }

        public void sendToAll(string name, string msg){
            Clients.All.InvokeAsync("sendToAll", name, msg );
        }
    }
}
